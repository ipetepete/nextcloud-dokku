# Dokku deployed nextcloud container

Setup steps

        dokku apps:create nextcloud
        dokku mariadb:create nextcloud_db
        dokku mariadb:link nextcloud nextcloud_db
        dokku domains:add nextcloud <domain name>
        dokku letsencrypt nextcloud # may need to remove default domains created by dokku

